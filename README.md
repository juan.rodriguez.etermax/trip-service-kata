Testing legacy code: Hard-wired dependencies
============================================
#### Exercise: 
* Add unit test (starts with TripService)
* When all the lines are covered (you can use Coverage tool) you can start the refactoring.
#### Constrains:
* We can't change any existing code if not covered by tests. 
* The only exception is if we need to change the code to add unit tests, but in this case, just automated refactorings (via IDE) are allowed.
#### Recomendations:
* Start testing from shortest to deepest branch
* Start refactoring from deepest to shortest branch